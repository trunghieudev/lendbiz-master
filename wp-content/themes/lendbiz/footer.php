<?php
/**
 * The template for displaying the footer
 */
	
	$post_option = financity_get_post_option(get_the_ID());
	if( empty($post_option['enable-footer']) || $post_option['enable-footer'] == 'default' ){
		$enable_footer = financity_get_option('general', 'enable-footer', 'enable');
	}else{
		$enable_footer = $post_option['enable-footer'];
	}	
	if( empty($post_option['enable-copyright']) || $post_option['enable-copyright'] == 'default' ){
		$enable_copyright = financity_get_option('general', 'enable-copyright', 'enable');
	}else{
		$enable_copyright = $post_option['enable-copyright'];
	}

	$fixed_footer = financity_get_option('general', 'fixed-footer', 'disable');
	echo '</div>'; // financity-page-wrapper

	if( $enable_footer == 'enable' || $enable_copyright == 'enable' ){

		if( $fixed_footer == 'enable' ){
			echo '</div>'; // financity-body-wrapper

			echo '<footer class="financity-fixed-footer" id="financity-fixed-footer" >';
		}else{
			echo '<footer>';
		}

		if( $enable_footer == 'enable' ){

			$financity_footer_layout = array(
				'footer-1'=>array('financity-column-60'),
				'footer-2'=>array('financity-column-15', 'financity-column-15', 'financity-column-15', 'financity-column-15'),
				'footer-3'=>array('financity-column-15', 'financity-column-15', 'financity-column-30',),
				'footer-4'=>array('financity-column-20', 'financity-column-20', 'financity-column-20'),
				'footer-5'=>array('financity-column-20', 'financity-column-40'),
				'footer-6'=>array('financity-column-40', 'financity-column-20'),
			);
			$footer_style = financity_get_option('general', 'footer-style');
			$footer_style = empty($footer_style)? 'footer-2': $footer_style;

			$count = 0;
			$has_widget = false;
			foreach( $financity_footer_layout[$footer_style] as $layout ){ $count++;
				if( is_active_sidebar('footer-' . $count) ){ $has_widget = true; }
			}

			if( $has_widget ){ 	
				$footer_column_divider = financity_get_option('general', 'enable-footer-column-divider', 'enable');
				$extra_class  = ($footer_column_divider == 'enable')? ' financity-with-column-divider': '';

				echo '<div class="financity-footer-wrapper ' . esc_attr($extra_class) . '" >';
				echo '<div class="financity-footer-container financity-container clearfix" >';
				echo '<div class="row">';//row
				echo '<div class="col-md-3 col-sm-12">';;// column1 END
					if( is_active_sidebar('footer-1')){
						echo '<div class="financity-footer-column financity-item-pdlr" >';
							dynamic_sidebar('footer-1'); 
						echo '</div>';
					}
				echo '</div>';// column1 END
				// column2
				echo '<div class="col-md-9 col-sm-12">';
					$count = 1;
				
					echo '<div class="row">';
						foreach( $financity_footer_layout[$footer_style] as $layout ){ $count++;
							// var_dump($financity_footer_layout[$footer_style]);
							if( is_active_sidebar('footer-' . $count) ){
								echo '<div class="financity-footer-column financity-item-pdlr col-md-4 col-sm-12" >';
									dynamic_sidebar('footer-' . $count); 
								echo '</div>';
							}
						}
					echo '</div>';
					// copyright
					?>
						<?php if ( is_active_sidebar( 'copyright_1' ) ) : ?>
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'copyright_1' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>

					<?php
				echo '</div>'; // column2 End
				echo '</div>'; // Row
				echo '</div>'; // financity-footer-container
				echo '</div>'; // financity-footer-wrapper 
			}
		} // enable footer

		
		echo '</footer>';

		if( $fixed_footer == 'disable' ){
			echo '</div>'; // financity-body-wrapper
		}
		echo '</div>'; // financity-body-outer-wrapper

	// disable footer	
	}else{
		echo '</div>'; // financity-body-wrapper
		echo '</div>'; // financity-body-outer-wrapper
	}

	$header_style = financity_get_option('general', 'header-style', 'plain');
	
	if( $header_style == 'side' || $header_style == 'side-toggle' ){
		echo '</div>'; // financity-header-side-nav-content
	}

	$back_to_top = financity_get_option('general', 'enable-back-to-top', 'disable');
	if( $back_to_top == 'enable' ){
		echo '<a href="#financity-top-anchor" class="financity-footer-back-to-top-button" id="financity-footer-back-to-top-button"><i class="fa fa-angle-up" ></i></a>';
	}
?>

<?php wp_footer(); ?>

</body>
</html>