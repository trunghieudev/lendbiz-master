<?php
	/*	
	*	Goodlayers Item
	*/
	
	// [gdlr-core-accordion]
	// [gdlr_core_tab title="" ]
	// [/gdlr-core-accordion]
	add_shortcode('gdlr-core-accordion', 'gdlr_core_accordion_shortcode');
	if( !function_exists('gdlr_core_accordion_shortcode') ){
		function gdlr_core_accordion_shortcode($atts, $content = ''){
			$atts = shortcode_atts(array(), $atts, 'gdlr-core-accordion');
			
			$ret  = apply_filters('gdlr_core_accordion_content', $atts, $content);
			if( !empty($ret) ){ return $ret; }

			global $gdlr_core_tabs;
			$gdlr_core_tabs = array();

			do_shortcode($content);

			$active_tab = 0;

			$ret  = '<div class="gdlr-core-accordion-item gdlr-core-item-pdb gdlr-core-accordion-style-background-title-icon gdlr-core-icon-pos-left gdlr-core-left-align">';		
			
			$count = 0;		
			
			foreach( $gdlr_core_tabs as $gdlr_core_tab ){
				$ret .= '<div class="gdlr-core-accordion-item-tab clearfix ' . (($count == $active_tab)? 'gdlr-core-active': '') . '">';
				$ret .= '<div class="gdlr-core-accordion-item-content-wrapper">';
				$ret .= '<h4 class="gdlr-core-accordion-item-title gdlr-core-js gdlr-core-skin-e-background gdlr-core-skin-e-content" >' . $gdlr_core_tab['title'] . '</h4>';
				$ret .= '<div class="gdlr-core-accordion-item-content">' . $gdlr_core_tab['content'] . '</div>';
				$ret .= '</div>';
				$ret .= '</div>';
				
				$count ++;
			}
			$ret .= '</div>'; // gdlr-core-accordion-item

			return $ret;
		}
	}