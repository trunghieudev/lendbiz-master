<?php

	// start your child theme code here
	
	// custom function to disable the trim excerpt
	// add_filter('wp_trim_excerpt', 'custom_disable_trim_excerpt', 11, 2);
	// function custom_disable_trim_excerpt( $excerpt, $original ){
	// 	if( !empty($original) ){
	// 		return $original;
	// 	}
	// 	return $excerpt;
	// }	

	// set 1 process for image importer
	// add_filter('gdlr_core_importer_image_per_process', 'custom_importer_image_per_process');
	// if( !function_exists('custom_importer_image_per_process') ){
	// 	function custom_importer_image_per_process(){
	// 		return 1000;
	// 	}
	// }

function my_scripts_method() {
    wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/js/custom_script.js',
        array( 'jquery' )
    );
}


add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
include('soap/soap.php');


/**
 * Register our sidebars and widgetized areas.
 *
 */
function copyright_1_widgets_init() {

	register_sidebar( array(
		'name'          => 'Copyright Customs',
		'id'            => 'copyright_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'copyright_1_widgets_init' );