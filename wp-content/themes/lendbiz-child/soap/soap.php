<?php

add_action('caldera_forms_submit_complete', 'send_request_caresoft_via_caldera');
//function function_alert($message) {
    // Display the alert box 
//    echo "<script>alert('$message');</script>";
//}
function send_request_caresoft_via_caldera()
{
	// Form đăng ký đầu tư
	if ( $_POST["_cf_frm_id"] == "CF5eb10a6ea5bcd")
	{	
		$name = $_POST["fld_1231522"];
		$email = $_POST["fld_2381103"];
		$phone = $_POST["fld_1091422"];
		$code = $_POST["fld_976597"];
		$social = $_POST["fld_4144953"];

		$soapUrl = "https://bps.lendbiz.vn/BPS/OnlineTrading.asmx?wsdl"; 

		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<ReqJoin xmlns="http://tempuri.org">
			  <pv_Type>I</pv_Type>
			  <pv_Fullname>'.$name.'</pv_Fullname>
			  <Pv_Mobile>'.$phone.'</Pv_Mobile>
			  <Pv_Email>'.$email.'</Pv_Email>
			  <Pv_Online>'.$social.'</Pv_Online>
			  <Pv_ConsultalInfo>'.$code.'</Pv_ConsultalInfo>
			  <Pv_MatchOrdSMS>Y</Pv_MatchOrdSMS>
			</ReqJoin>
		  </soap:Body>
		</soap:Envelope>';

		$headers = array(
			"Content-Type: text/xml; charset=utf-8",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: http://tempuri.org/ReqJoin", 
			"Content-length: ".strlen($xml_post_string),
		);
		
		//caia_log('soap', 'array_data', $xml_post_string);
        // function_alert($xml_post_string);
		$url = $soapUrl;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch); 
		curl_close($ch);
		
		//caia_log('soap', 'array_data1', $response);
        //function_alert($response);
	}
	
	// Form cần được đầu tư
	if ( $_POST["_cf_frm_id"] == "CF5eb10bb2cf520" )
	{
		$name = $_POST["fld_1231522"];
		$email = $_POST["fld_2381103"];
		$phone = $_POST["fld_1091422"];
		$city = $_POST["fld_976597"];
		$who = $_POST["fld_3266158"];
		$social = $_POST["fld_4144953"];
		$token = date("YmdHis");
		$soapUrl = "https://bps.lendbiz.vn/BPS/OnlineTrading.asmx?wsdl"; 

		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<ReqJoin xmlns="http://tempuri.org">
			  <pv_Type>B</pv_Type>
			  <pv_Fullname>'.$name.'</pv_Fullname>
			  <Pv_Mobile>'.$phone.'</Pv_Mobile>
			  <Pv_Email>'.$email.'</Pv_Email>
			  <Pv_Address>'.$city.'</Pv_Address>		
			  <Pv_Job>'.$who.'</Pv_Job> 
			  <Pv_Online>'.$social.'</Pv_Online>
			  <Pv_MatchOrdSMS>Y</Pv_MatchOrdSMS>
			</ReqJoin>
		  </soap:Body>
		</soap:Envelope>';
		$headers = array(
			"Content-Type: text/xml; charset=utf-8",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: http://tempuri.org/ReqJoin", 
			"Content-length: ".strlen($xml_post_string),
		);
		
		//caia_log('soap', 'array_data', $xml_post_string);
        // function_alert($xml_post_string);
		$url = $soapUrl;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch); 
		curl_close($ch);
		
		
		//caia_log('soap', 'array_data1', $response);
	}
	
	// Form phát triển đối tác
	if ( $_POST["_cf_frm_id"] == "CF5eb10d26b1e32" )
	{	
		$name = $_POST["fld_1231522"];
		$email = $_POST["fld_2381103"];
		$phone = $_POST["fld_1091422"];
		$job = $_POST["fld_3266158"];


		$soapUrl = "https://bps.lendbiz.vn/BPS/OnlineTrading.asmx?wsdl"; 

		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<ReqJoin xmlns="http://tempuri.org">
			  <pv_Type>C</pv_Type>
			  <pv_Fullname>'.$name.'</pv_Fullname>
			  <Pv_Mobile>'.$phone.'</Pv_Mobile>
			  <Pv_Email>'.$email.'</Pv_Email>
			  <Pv_MatchOrdSMS>Y</Pv_MatchOrdSMS>
			</ReqJoin>
		  </soap:Body>
		</soap:Envelope>';

		$headers = array(
			"Content-Type: text/xml; charset=utf-8",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: http://tempuri.org/ReqJoin", 
			"Content-length: ".strlen($xml_post_string),
		);
		
		//caia_log('soap', 'array_data', $xml_post_string);
        // function_alert($xml_post_string);
		$url = $soapUrl;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch); 
		curl_close($ch);
		
		//caia_log('soap', 'array_data1', $response);

	}
	
	// Form liên hệ
	if ( $_POST["_cf_frm_id"] == "CF5e7318f9dd660" )
	{	
		$name = $_POST["fld_3675890"];
		$email = $_POST["fld_2587502"];
		$phone = $_POST["fld_5445156"];
		$text = $_POST["fld_3885959"];

		$soapUrl = "https://bps.lendbiz.vn/BPS/OnlineTrading.asmx"; 

		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<ReqJoin xmlns="http://tempuri.org">
			  <pv_Type>I</pv_Type>
			  <pv_Fullname>'.$name.'</pv_Fullname>
			  <Pv_Mobile>'.$phone.'</Pv_Mobile>
			  <Pv_Email>'.$email.'</Pv_Email>
			  <Pv_Job>'.$text.'</Pv_Job>
			  <Pv_MatchOrdSMS>Y</Pv_MatchOrdSMS>
			</ReqJoin>
		  </soap:Body>
		</soap:Envelope>';

		$headers = array(
			"Content-Type: text/xml; charset=utf-8",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: http://tempuri.org/ReqJoin", 
			"Content-length: ".strlen($xml_post_string),
		);
		
		//caia_log('soap', 'array_data', $xml_post_string);

		$url = $soapUrl;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch); 
		curl_close($ch);
		
		//caia_log('soap', 'array_data1', $response);

	}
	
	// Form nhận thông tin
	if ( $_POST["_cf_frm_id"] == "CF5ecf1a28dff27" )
	{	
		$name = $_POST["fld_5698804"];
		$email = $_POST["fld_2324895"];
		$phone = $_POST["fld_4445566"];

		$soapUrl = "https://bps.lendbiz.vn/BPS/OnlineTrading.asmx"; 

		$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<ReqJoin xmlns="http://tempuri.org">
			  <pv_Type>I</pv_Type>
			  <pv_Fullname>'.$name.'</pv_Fullname>
			  <Pv_Mobile>'.$phone.'</Pv_Mobile>
			  <Pv_Email>'.$email.'</Pv_Email>
			  <Pv_MatchOrdSMS>Y</Pv_MatchOrdSMS>
			</ReqJoin>
		  </soap:Body>
		</soap:Envelope>';

		$headers = array(
			"Content-Type: text/xml; charset=utf-8",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: http://tempuri.org/ReqJoin", 
			"Content-length: ".strlen($xml_post_string),
		);
		
		//caia_log('soap', 'array_data', $xml_post_string);

		$url = $soapUrl;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch); 
		curl_close($ch);
		
		//caia_log('soap', 'array_data1', $response);

	}
}

