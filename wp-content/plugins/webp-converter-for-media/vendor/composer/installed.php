<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'fc539b18cc283e9063bdb3b9ec0badab031b4317',
        'name' => 'gbiorczyk/webp-converter-for-media',
        'dev' => false,
    ),
    'versions' => array(
        'gbiorczyk/webp-converter-for-media' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'fc539b18cc283e9063bdb3b9ec0badab031b4317',
            'dev_requirement' => false,
        ),
        'matt-plugins/deactivation-modal' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../matt-plugins/deactivation-modal',
            'aliases' => array(),
            'reference' => 'e4382cd530992d7fb4b6e0938eda06230e18e3b5',
            'dev_requirement' => false,
        ),
    ),
);
